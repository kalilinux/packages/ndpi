ndpi (1.7-0kali1) kali-dev; urgency=medium

  * Imported upstream version 1.7
  * Bump SONAME to 3 and update symbol file.
  * Disable example-link-dynamic.patch as it's not compiled with this patch

 -- Sophie Brun <sophie@freexian.com>  Thu, 05 Nov 2015 15:49:28 +0100

ndpi (1.6-1) unstable; urgency=low

  * Imported Upstream version 1.6
  * Update copyright with the removal of embedded json-c.
  * Remove pcap-linking.patch and export-additional-symbols.patch (merged
    upstream).
  * Update json-c-use-system.patch and example-link-dynamic.patch for new
    upstream code.
  * Update debian/watch file for new upstrm tarball name.
  * Update docs and copyright.
  * Bump SONAME to 2 and update symbol file.

 -- Ludovico Cavedon <cavedon@debian.org>  Tue, 09 Sep 2014 16:55:57 -0700

ndpi (1.5.0-1) unstable; urgency=medium

  * Imported Upstream version 1.5.0
  * Update copyright.
  * Add README.Debian to point to upstream documentation.
  * Update library symbols.
  * Update Standards-Version to 3.9.5.
  * Add ndpiReader.c example.
  * Build ndpiReader into separate package libndpi-bin
  * Update watch file for new stable release.
  * Update debian/rules get-orig-source to support invoking uscan.
  * Rename library package to libndpi1a.
  * Remove all previous patches.
  * Add example-link-dynamic.patch for dynamically linking examples against
    libndpi.
  * Add json-c-use-system.patch for using system libjson-c.
  * Add pcap-linking.patch to avoid linking libndpi against libpcap.
  * Include ndpiReader in libndpi-bin package.
  * Add man page for ndpiReader.
  * Add export-additional-symbols.patch to export ndpi_set_proto_defaults
    symbol.
  * Add Build-Depends on libpcap-dev and pkg-config.
  * Add libndpi-dbg package with debug symbols.

 -- Ludovico Cavedon <cavedon@debian.org>  Thu, 14 Aug 2014 00:12:37 -0700

ndpi (1.4.0+svn6932-1) unstable; urgency=low

  * New upstream release (synced with ntopng 1.1).
  * Update symbols file.
  * Remove unused substitution variable in control file.

 -- Ludovico Cavedon <cavedon@debian.org>  Mon, 11 Nov 2013 23:20:11 -0800

ndpi (1.4.0+svn6712-1) unstable; urgency=low

  * Initial release. Thanks to Raphaël Hertzog. (Closes: #721551)

 -- Ludovico Cavedon <cavedon@debian.org>  Sun, 01 Sep 2013 23:06:24 +0200
